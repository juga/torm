# torm

torm or Tor [ORM], is a library to parse [Tor descriptors] files
to a DBMS.

## Status

This is a WIP project for Tor hackweek 2022, atm only the [models](#models) are
implemented.

There're partial implemetations for parsing descriptors:

- arti parses router descriptors: <https://docs.rs/tor-netdoc/0.4.1/tor_netdoc/doc/routerdesc/index.html>
- some crates parsing bridges descriptors: <https://gitlab.torproject.org/trinity-1686a/collector-processing/-/tree/main/collector/src/descriptor/kind>

Would it be possible to use [serde serialize]?

## Rust ORM libraries comparative

- <https://libraries.io/search?languages=Rust&q=orm&sort=dependents_count>

- <https://github.com/rbatis/rbatis#why-not-diesel-or-not-sqlx>

- <https://www.libhunt.com/compare-sea-orm-vs-rbatis>

Choosing [SeaORM] cause it's possible to
generate the models from an existing DB.

## Models

The dir `entities/src` contains the entities generated from [onbasca] and
[bwauthealth] DBs with the command:  `sea-orm-cli generate entity -u <DB URI>`

DB_URI is for example:

- `sqlite:///dbfile.sql`
- `postgresql://<user>:<pass>@<ip>:<port>/<dbname>`

## Versions

Tested only with cargo and rustc 1.60.0

[bwauthealth]: https://gitlab.torproject.org/juga/bwauthealth
[DBMS]: https://en.wikipedia.org/wiki/Database#Database_management_system
[onbasca]: https://gitlab.torproject.org/tpo/network-health/onbasca
[ORM]: https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping
[serde serialize]: https://docs.serde.rs/serde/trait.Serialize.html
[SeaORM]: (https://www.sea-ql.org/SeaORM/)
[Tor descriptors]: <https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/dir-spec.txt>
