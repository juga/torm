//! SeaORM Entity. Generated by sea-orm-codegen 0.8.0

use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "consensus")]
pub struct Model {
    #[sea_orm(column_name = "_obj_created_at")]
    pub obj_created_at: Option<DateTime>,
    #[sea_orm(column_name = "_obj_updated_at")]
    pub obj_updated_at: Option<DateTime>,
    #[sea_orm(primary_key, auto_increment = false)]
    pub valid_after: DateTime,
    #[sea_orm(column_name = "_exits_min_bandwidth")]
    pub exits_min_bandwidth: Option<i32>,
    #[sea_orm(column_name = "_exits_min_position")]
    pub exits_min_position: Option<i32>,
    #[sea_orm(column_name = "_non_exits_min_bandwidth")]
    pub non_exits_min_bandwidth: Option<i32>,
    #[sea_orm(column_name = "_non_exits_min_position")]
    pub non_exits_min_position: Option<i32>,
    #[sea_orm(column_name = "_cc_alg_2")]
    pub cc_alg_2: Option<bool>,
    #[sea_orm(column_name = "_bwscanner_cc_gte_1")]
    pub bwscanner_cc_gte_1: Option<bool>,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::bwfile::Entity")]
    Bwfile,
    #[sea_orm(has_many = "super::relay_consensuses::Entity")]
    RelayConsensuses,
    #[sea_orm(has_many = "super::routerstatus::Entity")]
    Routerstatus,
}

impl Related<super::bwfile::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Bwfile.def()
    }
}

impl Related<super::relay_consensuses::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::RelayConsensuses.def()
    }
}

impl Related<super::routerstatus::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Routerstatus.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
